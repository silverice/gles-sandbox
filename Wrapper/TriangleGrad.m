//
//  TriangleGrad.m
//  Wrapper
//
//  Created by Denis Halabuzar on 2/13/13.
//  Copyright (c) 2013 IDAP Group. All rights reserved.
//

#import "TriangleGrad.h"

@interface TriangleGrad ()

@property (nonatomic, retain)   IDPShader   *shader;
@property (retain)  IDPVertexArray  *array;

@end

@implementation TriangleGrad {
    GLuint oesBuffer, vertBuff;
}

@synthesize shader      = _shader;

- (void)dealloc{
    [self dispose];
    [super dealloc];
}

typedef struct {
    GLKVector2 coord;
    GLKVector4 color;
} Vert;

static const Vert coords[] = {
    -1, -1,     0,0,0,1,
    -1, 1,     0,0,0,1,
    1, -1,     0,0,1,1,
    1, 1,     0,0,1,1,
};

- (id)init
{
    self = [super init];
    if (self) {
        self.shader = [[[IDPShader alloc] initWithVertexShader:@"TriangleGrad"
                                                fragmentShader:@"TriangleGrad"] autorelease];
        [self.shader bindAttribute:"position" toLocation:GLKVertexAttribPosition];
        //[self.shader bindAttribute:"colors" toLocation:GLKVertexAttribColor];
        [self.shader link];
        
//        GLKMatrix4 proj = GLKMatrix4MakeOrtho(-1, 1, -1, 1, 1, -1);
//        GLuint projLocation = [self.shader uniformLocation:"Projection"];
//        [self.shader use];
//        glUniformMatrix4fv(projLocation, 1, 0, proj.m);
        
        self.array = [IDPVertexArray objectWithUsage:GL_STATIC_DRAW
                                                data:coords
                                            dataSize:sizeof(coords)
                                         elementSize:elementSize(coords)];
        
        [self.array describeStructWithIdentifier:GLKVertexAttribPosition
                                    elementCount:2
                                     elementType:GL_FLOAT
                                      normalized:NO
                                       ptrOffset:0];
        
        [self.array describeStructWithIdentifier:GLKVertexAttribColor
                                    elementCount:4
                                     elementType:GL_FLOAT
                                      normalized:NO
                                       ptrOffset:(void*)sizeof(GLKVector2)];
        
//        glGenVertexArraysOES(1, &oesBuffer);
//        glBindVertexArrayOES(oesBuffer);
//        glGenBuffers(1, &vertBuff);
//        glBindBuffer(GL_ARRAY_BUFFER, vertBuff);
//        
//        glBufferData(GL_ARRAY_BUFFER, sizeof(coords), coords, GL_STATIC_DRAW);
//        
//        glEnableVertexAttribArray(GLKVertexAttribPosition);
//        glVertexAttribPointer(GLKVertexAttribPosition, 2, GL_FLOAT, 0, sizeof(coords[0]), 0);
//        
//        glEnableVertexAttribArray(GLKVertexAttribColor);
//        glVertexAttribPointer(GLKVertexAttribColor, 4, GL_FLOAT, 0, sizeof(coords[0]), (void*)sizeof(GLKVector2));
//        
//        glBindVertexArrayOES(0);
    }
    return self;
}

- (void)dispose {
    self.array = nil;
    //glDeleteVertexArraysOES(1, &oesBuffer);
    //glDeleteBuffers(1, &vertBuff);
    self.shader = nil;
}

- (void)draw {
    [self.shader use];
    [self.array bindAndDraw];
   // glBindVertexArrayOES(oesBuffer);
   // glDrawArrays(GL_TRIANGLE_STRIP, 0, countOf(coords));
}

@end
