//
//  TextureDrawer.h
//  Wrapper
//
//  Created by Denis Halabuzar on 2/11/13.
//  Copyright (c) 2013 IDAP Group. All rights reserved.
//

#import "IDPDrawable.h"

@interface MovingPyramide : NSObject <IDrawable>

@property (nonatomic, assign)   GLfloat     pitch;
@property (nonatomic, assign)   GLfloat     yaw;

@end
