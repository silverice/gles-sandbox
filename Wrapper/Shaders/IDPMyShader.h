//
//  IDPMyShaderProgram.h
//  Wrapper
//
//  Created by Denis Halabuzar on 2/11/13.
//  Copyright (c) 2013 IDAP Group. All rights reserved.
//

#import "IDPShader.h"
#import <GLKit/GLKit.h>

@interface IDPMyShader : IDPShader

@property (nonatomic, assign)   GLKMatrix4  projection;

@end
