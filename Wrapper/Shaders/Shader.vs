//
//  Shader.vsh
//  Wrapper
//
//  Created by Denis Halabuzar on 2/8/13.
//  Copyright (c) 2013 IDAP Group. All rights reserved.
//

attribute vec4 position;
attribute vec4 color;

varying highp vec4 colorVarying;

uniform mat4 modelViewProjectionMatrix;
uniform vec4 defaultColor;

void main()
{
    colorVarying = color;
    gl_Position = modelViewProjectionMatrix * position;
}
