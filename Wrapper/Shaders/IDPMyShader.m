//
//  IDPMyShaderProgram.m
//  Wrapper
//
//  Created by Denis Halabuzar on 2/11/13.
//  Copyright (c) 2013 IDAP Group. All rights reserved.
//

#import "IDPMyShader.h"

@implementation IDPMyShader {
    
}

+ (id)make {
    NSString *vertShaderPathname = [[NSBundle mainBundle] pathForResource:@"Shader" ofType:@"vs"];
    NSString *fragShaderPathname = [[NSBundle mainBundle] pathForResource:@"Shader" ofType:@"fs"];
    
    IDPMyShader *me = [[[self alloc] initWithVertexShader:vertShaderPathname
                                          fragmentShader:fragShaderPathname] autorelease];
    
    return me;
}

@end
