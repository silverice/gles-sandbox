//
//  Shader.fsh
//  Wrapper
//
//  Created by Denis Halabuzar on 2/8/13.
//  Copyright (c) 2013 IDAP Group. All rights reserved.
//

varying highp vec4 colorVarying;

uniform sampler2D texture;

void main()
{
    gl_FragColor = colorVarying;//texture2D(texture, vec2(gl_FragCoord));
}
