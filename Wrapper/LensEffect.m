
#import "LensEffect.h"


@implementation LensEffect

- (id)init
{
    self = [super initWithShader:@"Lens" andTexture:@"grid.png"];
    if (self) {
        
    }
    return self;
}

- (void)onTouch:(CGPoint)point {
    [self.shader setUniform:@"center" to2f:point.x :point.y];
}

@end
