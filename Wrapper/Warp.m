//
//  Warp.m
//  Wrapper
//
//  Created by Denis Halabuzar on 2/12/13.
//  Copyright (c) 2013 IDAP Group. All rights reserved.
//

#import "Warp.h"

@interface Warp ()
@property (nonatomic, retain) TextureEffectBase     *effect;
@end

@implementation Warp {
    GLuint _frameBuffer, _renderBuffer;
    Byte *pixels;
}

@synthesize effect  = _effect;

- (void)setupFramebufferObject {
    pixels = malloc(self.texture.size.x * self.texture.size.y * 4);
    memset_pattern4(pixels, (Byte[]){150,33,20,255}, self.texture.size.x * self.texture.size.y * 4);
    
    glGenFramebuffers(1, &_frameBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, _frameBuffer);
    
    glGenRenderbuffers(1, &_renderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, _renderBuffer);
    
    glRenderbufferStorage(GL_RENDERBUFFER, GL_RGBA8_OES, self.texture.size.x, self.texture.size.y);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, _renderBuffer);
    
    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER) ;
    if(status != GL_FRAMEBUFFER_COMPLETE) {
        NSLog(@"failed to make complete framebuffer object %x", status);
        assert(false);
    }
    //glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

- (id)init {
    NSString *path = @"imgres.png";
    checkGL
    self = [super initWithShader:@"Warp"
                      andTexture:path
                     textureSlot:GL_TEXTURE0];
    if (self) {
        [self setCenter:GLKVector2Make(0, 0)];
        [self onTouch:CGPointMake(0, 0)];
        [self setupFramebufferObject];
        checkGL
        
        self.effect = [[[TextureEffectBase alloc] initWithShader:@"TextureFShader"
                                                      andTexture:path
                                                     textureSlot:GL_TEXTURE1] autorelease];
    }
    return self;
}

- (void)onTouch:(CGPoint)point {
    [self.shader setUniform:@"end" to2f:point.x :point.y];
}

- (void)onTouchBegin:(CGPoint)point {
    [self onTouch:point];
    [self setCenter:GLKVector2Make(point.x, point.y)];
}

- (void)setCenter:(GLKVector2)center {
    [self.shader setUniform:@"center" to2fv:center.v count:1];
}

- (void)draw {
    checkGL
    GLint oldBuff;
    glGetIntegerv(GL_FRAMEBUFFER_BINDING, &oldBuff);
    glBindFramebuffer(GL_FRAMEBUFFER, _frameBuffer);
    {
//        checkGL
//        glClearColor(0, 1, 0, 1);
//        //glViewport(0, 0, 256, 256);
//        [super draw];
//        //glPixelStorei(GL_PACK_ALIGNMENT, 1);
//        checkGL
//        glReadPixels(0, 0, 256, 256, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
    }
    glBindFramebuffer(GL_FRAMEBUFFER, oldBuff);
    checkGL
    
//    [self.effect.texture bind];
//    [self.effect.texture updateData:pixels];
//    [self.effect draw];
//    checkGL
    
    [super draw];
    glReadPixels(0, 0, 256, 256, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
    
    
    checkGL
    for (int i = 0; i < 256*256*4; ) {
        if (pixels[i] != 0) {
            //NSLog(@"not nill at %u", i);
        }
        i += 4;
    }

    checkGL
}

@end
