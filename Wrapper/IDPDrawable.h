//
//  IDrawable.h
//  Wrapper
//
//  Created by Denis Halabuzar on 2/11/13.
//  Copyright (c) 2013 IDAP Group. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GLKit/GLKit.h>

#import "IDPGLTexture.h"
#import "IDPShader.h"
#import "IDPVertexArray.h"

#import "GLProgram.h"
#import "GLTexture.h"
#import "GLVertexArray.h"

@protocol IDrawable <NSObject>
@required
- (void)draw;
- (void)dispose;

// interaction
@optional
- (void)onTouch:(CGPoint)point;
- (void)onTouchBegin:(CGPoint)point;

@end

#define countOf(array) (sizeof(array)/sizeof(array[0]))

#define elementSize(array) (sizeof(array[0]))

#define checkGL     {   \
        GLenum error = glGetError();    \
        if (error != GL_NO_ERROR) {     \
            NSLog(@"gl error: 0x%x", error);    \
            assert(false);  \
        }   \
    }

