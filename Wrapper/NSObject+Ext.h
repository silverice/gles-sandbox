//
//  NSObject+Ext.h
//  Wrapper
//
//  Created by Denis Halabuzar on 9/19/13.
//  Copyright (c) 2013 IDAP Group. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Ext)

+ (id)object;

- (id)autocopy;
- (id)autocopymutable;

@end
