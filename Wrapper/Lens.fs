//
//  TextureDrawer.fs
//  Wrapper
//
//  Created by Denis Halabuzar on 2/12/13.
//  Copyright (c) 2013 IDAP Group. All rights reserved.
//

precision highp float;

uniform sampler2D texture;
varying highp vec2 textureCoord;

uniform highp vec2 center;

highp float radius = 0.15;
highp float forceCoeff = 0.4;

float sphere(in float x) {
    float tangent = -2.0*x / sqrt(radius * radius - x * x);
    float angle = atan(tangent);
    float h = sqrt(radius * radius - x * x);
    
    return (1.0 - cos(angle)) * h / tangent;
}

void main(void) {
    highp vec2 x = textureCoord;
    highp vec2 cx = x - center;
    
    bool inRange = (radius > length(cx));
    float force = sphere(length(cx));
    
    highp vec2 shift = normalize(cx) * force * forceCoeff * float(inRange);
    
    if (inRange)
        gl_FragColor = texture2D(texture, x + shift);
    else
        gl_FragColor = texture2D(texture, x);
}