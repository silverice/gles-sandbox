//
//  TriangleGrad.h
//  Wrapper
//
//  Created by Denis Halabuzar on 2/13/13.
//  Copyright (c) 2013 IDAP Group. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IDPDrawable.h"

@interface TriangleGrad : NSObject < IDrawable >

@end
