//
//  IDPVertexArray.m
//  Wrapper
//
//  Created by Denis Halabuzar on 9/19/13.
//  Copyright (c) 2013 IDAP Group. All rights reserved.
//

#import "IDPVertexArray.h"

@interface IDPBufferObject ()
@property (nonatomic, assign)   GLuint uId;

@end

@implementation IDPBufferObject

- (void)dealloc {
    glDeleteBuffers(1, &_uId);
    [super dealloc];
}

- (void)bind {
    glBindBuffer(GL_ARRAY_BUFFER, self.uId);
}

- (void)modifyData:(const GLvoid *)data
          withSize:(GLsizei)size
          atOffset:(GLintptr)offset
{
    glBufferSubData(GL_ARRAY_BUFFER, offset, size, data);
}

- (void)setData:(const GLvoid *)data
       withSize:(GLsizei)size
      withUsage:(GLenum)usage
{
    glBufferData(GL_ARRAY_BUFFER, size, data, usage);
}

+ (id)objectWithUsage:(GLenum)usage {
    IDPBufferObject *me = [self object];
    if (me) {
        glGenBuffers(1, &me->_uId);
    }
    return me;
}

+ (id)objectWithUsage:(GLenum)usage
                 data:(const GLvoid *)data
                 size:(GLsizei)size
{
    IDPBufferObject *me = [self objectWithUsage:usage];
    if (me) {
        [me bind];
        glBufferData(GL_ARRAY_BUFFER, size, data, usage);
    }

    return me;
}

@end

@interface IDPVertexArray ()
@property (retain)  IDPBufferObject *buffer;
@property (assign)  NSUInteger      uId;
@property (assign)  NSUInteger      elementSize;
@property (assign)  NSUInteger      dataSize;
@end

@implementation IDPVertexArray

- (void)dealloc {
    self.buffer = nil;
    glDeleteVertexArraysOES(1, &_uId);
    [super dealloc];
}

+ (id)objectWithUsage:(GLenum)usage
                 data:(const GLvoid *)data
             dataSize:(GLsizei)dataSize
          elementSize:(GLsizei)elementSize
{
    assert((dataSize % elementSize) == 0);
    assert(elementSize > 0);
    assert(dataSize > 0);
    assert(data);
    
    IDPVertexArray *me = [self object];
    if (me) {
        me.elementSize = elementSize;
        me.dataSize = dataSize;
        
        glGenVertexArraysOES(1, &me->_uId);
        [me bind];
        
        me.buffer = [IDPBufferObject objectWithUsage:usage data:data size:dataSize];
        [me.buffer bind];
    }
    
    return me;
}

- (NSUInteger)vertexCount {
    return self.dataSize / self.elementSize;
}

- (void)describeStructures:(const IDPVertexArrayStructDecription*)descriptors
               structCount:(NSUInteger)count
{
    for (NSUInteger i = 0; i < count; ++i) {
        const IDPVertexArrayStructDecription *descr = &descriptors[i];
        glEnableVertexAttribArray(descr->identifier);
        glVertexAttribPointer(descr->identifier,
                              descr->elementCount,
                              descr->elementType,
                              descr->normalized,
                              self.elementSize,
                              descr->ptrOffset);
    }
}

- (void)describeStructWithIdentifier:(GLint)attribute
     elementCount:(GLsizei)size
                elementType:(GLenum)type
          normalized:(GLboolean)normalized
           ptrOffset:(const GLvoid *)ptrOffset
{
    glEnableVertexAttribArray(attribute);
    glVertexAttribPointer(attribute, size, type, normalized, self.elementSize, ptrOffset);
}

- (void)bind {
    glBindVertexArrayOES(self.uId);
}

- (void)bindAndDraw {
    [self bind];
    glDrawArrays(GL_TRIANGLE_STRIP, 0, self.dataSize / self.elementSize);
}

- (void)draw:(GLenum)mode {
    [self bind];
    glDrawArrays(mode, 0, self.dataSize / self.elementSize);
}

- (void)draw:(GLenum)mode from:(NSUInteger)from count:(NSUInteger)count {
    assert(from + count <= self.vertexCount);
    [self bind];
    glDrawArrays(mode, from, count);
}

@end

















