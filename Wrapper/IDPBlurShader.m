//
//  IDPBlurShader.m
//  Wrapper
//
//  Created by Denis Halabuzar on 2/14/13.
//  Copyright (c) 2013 IDAP Group. All rights reserved.
//

#import "IDPBlurShader.h"
#import "IDPGLTexture.h"

@implementation IDPBlurShader

@synthesize texture     = _texture;

+ (id)create {
    IDPBlurShader *me = [[[self alloc] initWithVertexShader:@"Blur" fragmentShader:@"Blur"] autorelease];
    if (me) {
        
    }
    
    return me;
}

- (void)dealloc {
    self.texture = nil;
    [super dealloc];
}

- (void)setTexture:(IDPGLTexture *)texture {
    [self use];
    GLuint loc = [self uniformLocation:"texture"];
    glUniform1i(loc, texture);
}


@end
