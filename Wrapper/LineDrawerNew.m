//
//  LineDrawerNew.m
//  Wrapper
//
//  Created by Denis Halabuzar on 10/14/13.
//  Copyright (c) 2013 IDAP Group. All rights reserved.
//

#import "LineDrawerNew.h"
#import "IDPSymbolVertexGen.h"

@interface LineDrawerNew ()
@property (retain)      IDPVertexArray *array;
@property (retain)      IDPShader *shader;
@property (retain)      NSMutableData   *points;
@property (retain)      IDPGLTexture    *texture;
@property (assign)      BOOL    shouldUpdate;
@end

@implementation LineDrawerNew

- (void)dealloc {
    self.array = nil;
    self.shader = nil;
    self.texture = nil;
    [super dealloc];
}

- (id)init
{
    self = [super init];
    if (self) {
        self.points = [NSMutableData data];
        
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glEnable( GL_BLEND );
        
        CGPoint points[] = {
            0,0,
            10,10,
            50,50,
            90,90,
            100, 110,
        };
        
        NSMutableData *vertices = [STSymbolVertexGen createTriangleMeshFromPoints:points
                                                                       pointCount:countOf(points)
                                                                        lineWidth:10
                                                                 transparentWidth:3];
        
        
        enum {Coord, Color, TexCoord};
        
        const IDPVertexArrayStructDecription dataDescr[] = {
            Coord, 2, GL_FLOAT, NO, 0,
            Color, 4, GL_FLOAT, NO, offsetof(STSymbolVertice, color),
        };
        
        self.array = [IDPVertexArray objectWithUsage:GL_STATIC_DRAW
                                                data:vertices.bytes
                                            dataSize:vertices.length
                                         elementSize:sizeof(STSymbolVertice)];
        [self.array describeStructures:dataDescr
                           structCount:countOf(dataDescr)];
        
        self.shader = [[[IDPShader alloc] initWithVertexShader:@"LineDrawerNew"
                                                fragmentShader:@"LineDrawerNew"] autorelease];
        [self.shader bindAttribute:"point" toLocation:Coord];
        [self.shader bindAttribute:"color" toLocation:Color];
        [self.shader link];

        checkGL
        [self.shader use];
        GLKMatrix4 proj = GLKMatrix4MakeOrtho(0, 120, 0, 120, 1, -1);
        GLuint projLocation = [self.shader uniformLocation:"Projection"];
        glUniformMatrix4fv(projLocation, 1, 0, proj.m);
        
        glActiveTexture(GL_TEXTURE0);
        self.texture = [IDPGLTexture objectWithImagePath:@"imgres.png"];
        [self.texture bind];
        glUniform1i([self.shader uniformLocation:"texture"], 0);
        
        GLfloat bounds[] = {120,120};
        glUniform2fv([self.shader uniformLocation:"bounds"], 1, bounds);

        
    }
    return self;
}


- (void)updateBuffer {
    if ((self.points.length / sizeof(CGPoint)) < 2) {
        return;
    }
    
    if (self.shouldUpdate == NO) {
        return;
    }
    
    self.shouldUpdate = NO;
    
//    for (NSUInteger i = 0; i < (self.points.length / sizeof(CGPoint)); ++i) {
//        NSLog(@"%@", NSStringFromCGPoint(
//                                  ((CGPoint*)self.points.bytes)[i]
//              ));
//    }
//
    NSLog(@"point count %lu", self.points.length / sizeof(CGPoint));
    

    NSMutableData *vertices = [STSymbolVertexGen createTriangleMeshFromPoints:(const CGPoint*)self.points.bytes
                                         pointCount:self.points.length / sizeof(CGPoint)
                                          lineWidth:10
                                   transparentWidth:1];
    
    enum {Coord, Color, TexCoord};
    
    const IDPVertexArrayStructDecription dataDescr[] = {
        Coord, 2, GL_FLOAT, NO, 0,
        Color, 4, GL_FLOAT, NO, offsetof(STSymbolVertice, color),
    };
    
    self.array = [IDPVertexArray objectWithUsage:GL_STATIC_DRAW
                                            data:vertices.bytes
                                        dataSize:vertices.length
                                     elementSize:sizeof(STSymbolVertice)];
    [self.array describeStructures:dataDescr
                       structCount:countOf(dataDescr)];

    /*
    [STSymbolVertexGen createTriangleMeshFromPoints:(const CGPoint*)self.points.bytes
                                         pointCount:self.points.length / sizeof(CGPoint)
                                          lineWidth:1
                                   transparentWidth:1
                                         verticeOut:&vertices
                                       verticeCount:&vertCount];
    
    [self.array bind];
    //[self.array.buffer bind];
    [self.array.buffer setData:vertices
                      withSize:vertCount * sizeof(STSymbolVertice)
                     withUsage:GL_STATIC_DRAW];
    
    */

    
}

- (void)draw {
    [self updateBuffer];
    
    [self.shader use];
    [self.array bindAndDraw];
}

- (void)onTouch:(CGPoint)point {
    point.x *= 120;
    point.y *= 120;
    
    [self.points appendBytes:&point length:sizeof(CGPoint)];
    
    self.shouldUpdate = YES;
}

@end
