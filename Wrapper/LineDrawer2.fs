precision highp float;

uniform float time;

uniform sampler2D texture;

void main(void) {
   // gl_FragColor = vec4(0.9,0.9,1.,1.);
    vec4 color = texture2D(texture, gl_PointCoord);
    gl_FragColor = color * vec4(1.4,1.,2.,1.0);
}