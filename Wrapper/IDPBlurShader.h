//
//  IDPBlurShader.h
//  Wrapper
//
//  Created by Denis Halabuzar on 2/14/13.
//  Copyright (c) 2013 IDAP Group. All rights reserved.
//

#import "IDPShader.h"

@class IDPGLTexture;

@interface IDPBlurShader : IDPShader

@property (nonatomic, retain)   IDPGLTexture    *texture;

+ (id)create;

@end
