//
//  TextureEffectBase.m
//
//
//  Created by Denis Halabuzar on 2/18/13.
//
//

#import "TextureEffectBase.h"

typedef struct {
    GLKVector2 coord;
    GLfloat color[4];
} Vert;

typedef struct {
    GLKVector3 coord;
    GLfloat color[4];
} Vert3;


static const GLKVector2 coords[] = {
    0, 0,
    0, 1,
    1, 0,
    1, 1,
};

@interface TextureEffectBase ()
@property (nonatomic, retain)   GLProgram       *shader;
@property (nonatomic, assign)   GLuint          textureSlot;
@property (nonatomic, retain)   IDPVertexArray  *array;
@end

@implementation TextureEffectBase

@synthesize texture = _texture;
@synthesize shader  = _shader;
@synthesize textureSlot = _textureSlot;

- (void)dispose {
    self.texture = nil;
    self.shader = nil;
    self.array = nil;
}

- (void)dealloc {
    [self dispose];
    [super dealloc];
}

- (id)initWithShader:(NSString *)shader
          andTexture:(NSString *)texture
{
    assert(self);
    return [self initWithShader:shader andTexture:texture textureSlot:GL_TEXTURE0];
}

//- (id)initWithShader:(NSString *)shader andTexture:(NSString *)texture {

- (id)initWithShader:(NSString *)shader
          andTexture:(NSString *)texture
         textureSlot:(GLuint)slot
{
    checkGL
    self = [super init];
    if (self) {
        {
            self.textureSlot = slot;
            
            self.shader = [GLProgram objectWithVertShaderName:@"TextureVShader.vs"
                                                   fragShader:shader
                                         linkedWithAttributes:@[@"Position", @(GLKVertexAttribPosition),
                                                                @"TexCoord", @(GLKVertexAttribTexCoord0)]];
            
            checkGL
            self.texture = [IDPGLTexture objectWithImagePath:texture];
            glActiveTexture(slot);
            [self.texture bind];
            
            [self.shader setUniform:@"texture" to1i:slot - GL_TEXTURE0];
            [self.shader setUniform:@"Projection" toMatrix4fv:1 transpose:NO value:GLKMatrix4MakeOrtho(0, 1, 0, 1, 1, -1).m];

            checkGL
        }
        
        checkGL
        
        self.array = [IDPVertexArray objectWithUsage:GL_STATIC_DRAW
                                                data:coords
                                            dataSize:sizeof(coords)
                                         elementSize:sizeof(coords[0])];
        
        
        [self.array describeStructWithIdentifier:GLKVertexAttribPosition
                                    elementCount:2
                                     elementType:GL_FLOAT
                                      normalized:GL_FALSE
                                       ptrOffset:0];
        
        [self.array describeStructWithIdentifier:GLKVertexAttribTexCoord0
                                    elementCount:2
                                     elementType:GL_FLOAT
                                      normalized:GL_FALSE
                                       ptrOffset:0];

        checkGL
    }
    return self;
}

- (void)draw {
    checkGL
    glActiveTexture(self.textureSlot);
    [self.texture bind];
    [self.shader bind];

    [self.array bindAndDraw];
    
    [self.shader unbind];
    [self.texture unbind];
    checkGL
}

@end
