//
//  TextureDrawer.m
//  Wrapper
//
//  Created by Denis Halabuzar on 2/11/13.
//  Copyright (c) 2013 IDAP Group. All rights reserved.
//

#import "MovingPyramide.h"
#import "IDPVertexArray.h"

#define offsetOf(Class, field) (&(((Class*)0)->field))

@interface MovingPyramide ()
@property (nonatomic, retain)   IDPGLTexture    *texture;
@property (nonatomic, retain)   IDPShader       *shader;
@property (nonatomic, retain)   IDPVertexArray  *vertextArray;
@end

@implementation MovingPyramide {
    GLuint oesCube, vertCube;
    GLuint uniformProjection;
    GLKMatrix4 projection;
}

@synthesize texture     = _texture;
@synthesize shader      = _shader;
@synthesize pitch       = _pitch;
@synthesize yaw         = _yaw;

- (void)dispose {
    glDeleteBuffers(1, &vertCube);
    glDeleteVertexArraysOES(1, &oesCube);
    self.texture = nil;
    self.shader = nil;
}

- (void)dealloc {
    [self dispose];
    [super dealloc];
}

typedef struct {
    GLKVector2 coord;
    GLfloat color[4];
} Vert;

typedef struct {
    GLKVector3 coord;
    GLfloat color[4];
} Vert3;


static const Vert coords[] = {
    0, 0,       1, 0, 0, 1,
    0, 100,     0, 1, 0, 1,
    100, 0,     1, 0, 0, 1,
    100, 100,   0, 0, 1, 1,
};

static const Vert3 pyr[] = {
    0, 0, 0,        0, 0, 0, 1,
    0.5, 0.5, 1,    0, 1, 1, 1,
    1, 0, 0,        0, 1, 0, 1,
    0.5, 1, 0,      0, 0, 1, 1,
    0, 0, 0,        1, 0, 0, 1,
    0.5, 0.5, 1,    1, 0, 1, 1,
};

- (id)init
{
    checkGL
    self = [super init];
    if (self) {
        
        self.texture = [IDPGLTexture objectWithImagePath:@"imgres.png"];
        
        {
            self.shader = [[[IDPShader alloc] initWithVertexShader:@"MovingPyramide"
                                                    fragmentShader:@"MovingPyramide"] autorelease];
            
            [self.shader bindAttribute:"Position" toLocation:GLKVertexAttribPosition];
            [self.shader bindAttribute:"SourceColor" toLocation:GLKVertexAttribColor];
            [self.shader link];
            checkGL
            
            [self.shader use];
            uniformProjection = [self.shader uniformLocation:"Projection"];
            
            checkGL
        }
        
        
        self.vertextArray = [IDPVertexArray objectWithUsage:GL_STATIC_DRAW
                                                       data:&pyr
                                                   dataSize:sizeof(pyr)
                                                elementSize:sizeof(pyr[0])];
        
        [self.vertextArray describeStructWithIdentifier:GLKVertexAttribPosition
                                           elementCount:3
                                            elementType:GL_FLOAT
                                             normalized:GL_FALSE
                                              ptrOffset:0];
        
        [self.vertextArray describeStructWithIdentifier:GLKVertexAttribColor
                                           elementCount:4
                                            elementType:GL_FLOAT
                                             normalized:GL_FALSE
                                              ptrOffset:(void*)offsetof(Vert3, color)];
        
        checkGL;
    }
    
    return self;
}

- (void)rotate {
    
    self.yaw += 0.005;
    self.pitch += 0.005;
    
    GLKMatrix4 baseModelViewMatrix = GLKMatrix4MakeTranslation(0.0f, 0.0f, -4.0f);
    baseModelViewMatrix = GLKMatrix4Rotate(baseModelViewMatrix, self.yaw, 0, 0, 1);
    
    GLKMatrix4 modelViewMatrix = GLKMatrix4MakeTranslation(0.0f, 0.0f, -1.5f);
    modelViewMatrix = GLKMatrix4Rotate(modelViewMatrix, self.pitch, 1, 0, 0);
    
    modelViewMatrix = GLKMatrix4Multiply(baseModelViewMatrix, modelViewMatrix);
    
    projection = GLKMatrix4MakePerspective(GLKMathDegreesToRadians(65.0f), 2/5.f, 0.01, 100);
    projection = GLKMatrix4Multiply(projection, modelViewMatrix);
    
    [self.shader use];
    glUniformMatrix4fv(uniformProjection, 1, 0, projection.m);
    
    //projection = GLKMatrix4MakeOrtho(0, 2.5, 0, 5, -1, 1);
}

- (void)draw {
    checkGL
    
    [self rotate];
    [self.vertextArray bindAndDraw];
    
    
    checkGL
}

- (void)onTouch:(CGPoint)point {
    self.yaw += point.x * 0.2;
    self.pitch += point.y * 0.2;
}


@end
