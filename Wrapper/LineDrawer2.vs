attribute float angle;
attribute float velocity;
attribute float size;

precision highp float;

uniform mat4 Projection;
uniform float time;

void main(void) {
    float x = cos(angle) * velocity * time;
    float y = sin(angle)  * velocity * time - 0.01 * time * time * 0.5;
   // x += sin(time * 3.0) * 10.;
    
    //float yVel = ;
    
    gl_Position = Projection * vec4(x, y, 1, 1);
    gl_PointSize = size;
}
