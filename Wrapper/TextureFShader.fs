


precision highp float;

uniform sampler2D texture;
varying highp vec2 textureCoord;

void main(void) {
    gl_FragColor = texture2D(texture, textureCoord);
}