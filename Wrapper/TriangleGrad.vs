attribute vec4 position;
attribute vec4 colors;

varying highp vec2 textureCoord;

uniform mat4 Projection;

void main(void) {
    textureCoord = vec2(position);
    gl_Position = position;
}
