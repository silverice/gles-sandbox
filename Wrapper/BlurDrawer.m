//
//  BlurDrawer.m
//  Wrapper
//
//  Created by Denis Halabuzar on 10/14/13.
//  Copyright (c) 2013 IDAP Group. All rights reserved.
//

#import "BlurDrawer.h"

@interface BlurDrawer ()

@end


static float const BlurKernelSize = 5;


@implementation BlurDrawer


float GaussDistrib2D(float x, float y, float cX, float cY) {
    float sigmaSq = 600;
    float A = 100;
    
    return A * exp(
                   -pow(x - cX, 2) / (2*sigmaSq) - pow(y - cY, 2) / (2*sigmaSq)
                   );
}

- (float *)creatGaussianDistribution {
    NSInteger size = BlurKernelSize * BlurKernelSize;
    float *textureData = calloc(size, sizeof(float));
    
    float summ = 0.0;
    
    for (int iY = 0; iY < BlurKernelSize; ++iY) {
        for (int iX = 0; iX < BlurKernelSize; ++iX) {
            GLfloat value =
            0.00001;//GaussDistrib2D(iX,                      iY,
                                  //         BlurKernelSize/2.f,      BlurKernelSize/2.f);
            
            printf(" %f", value);
            //NSLog(@"[%d , %d] : %f", iX, iY, value);

            textureData[iX + (int)(BlurKernelSize)*iY] = value;
            
            summ += value;
        }
        printf("\n");
    }
    NSLog(@"summ is %f", summ);
    

    
    for (NSInteger i = 0; i < size; ++i) {
        textureData[i] = textureData[i] / summ;
        NSLog(@" normaliz[%d] %f", i, textureData[i]);
    }
    
    return textureData;
}

- (id)init {
    NSString *path = @"imgres.png";
    checkGL
    self = [super initWithShader:@"blur"
                      andTexture:path
                     textureSlot:GL_TEXTURE0];
    if (self) {

        checkGL
        
        [self.shader bind];
        
        float *kerGaussBlur = [self creatGaussianDistribution];
        [self.shader setUniform:@"PSF" to1fv:kerGaussBlur count:BlurKernelSize*BlurKernelSize];
        free(kerGaussBlur);
        
        [self.shader setUniform:@"textureSize" to2f:1.f / self.texture.size.x :1.f / self.texture.size.y];
        
        GLint kernelOffset = (GLint)(BlurKernelSize / 2);
        [self.shader setUniform:@"kernelOffset" to1i:kernelOffset];
        
        [self.shader unbind];

    }
    return self;
}

- (void)draw {
    [super draw];
}

@end
