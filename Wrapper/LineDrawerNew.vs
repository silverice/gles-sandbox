attribute vec2 point;
attribute vec4 color;

precision highp float;

uniform mat4 Projection;

varying mediump vec4 colorV;
varying mediump vec2 vertCoordV;

void main(void) {
    colorV = color;
    vertCoordV = point;
    gl_Position = Projection * vec4(point.x, point.y, 1, 1);
}
