//
//  TextureEffectBase.h
//
//
//  Created by Denis Halabuzar on 2/18/13.
//
//

#import <Foundation/Foundation.h>
#import "IDPDrawable.h"
#import "IDPVertexArray.h"

@interface TextureEffectBase : NSObject < IDrawable >

@property (nonatomic, retain, readonly) GLProgram       *shader;
@property (nonatomic, retain)           IDPGLTexture    *texture;

- (id)initWithShader:(NSString *)shader
          andTexture:(NSString *)texture;

- (id)initWithShader:(NSString *)shader
          andTexture:(NSString *)texture
         textureSlot:(GLuint)slot;

@end
