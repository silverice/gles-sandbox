//
//  TextureDrawer.fs
//  Wrapper
//
//  Created by Denis Halabuzar on 2/12/13.
//  Copyright (c) 2013 IDAP Group. All rights reserved.
//

varying lowp vec4 DestinationColor; 

void main(void) { 
    gl_FragColor = DestinationColor;
}