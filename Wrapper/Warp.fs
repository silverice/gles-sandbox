//
//  TextureDrawer.fs
//  Wrapper
//
//  Created by Denis Halabuzar on 2/12/13.
//  Copyright (c) 2013 IDAP Group. All rights reserved.
//

precision highp float;

uniform sampler2D texture;

varying highp vec2 textureCoord;

uniform highp vec2 center;
uniform highp vec2 end;

highp float forceCoeff = 0.04;

float radiusX = 0.15;
float radiusY = 0.3;

float gauss(in float x, in float radius) {
    float c = radius;
    float part1 = -x * x / (c * c);
    float h = exp(part1);
    return h;
}

highp vec2 gauss2(in float x, in float radius) {

    float c = radius;
    
    float part1 = -x * x / (c * c);
    float tangent = exp(part1) * (-2.0 * x / (c*c));
    float h = exp(part1);
    
    float angle = atan(tangent);
    return vec2((1.0 - cos(angle)) * h / tangent, h);
}

vec2 rotate(in vec2 v, in float angle) {
    // tan = sin / cos, dy is sin
    return vec2(v.x * cos(angle) - v.y * sin(angle),
                v.y * cos(angle) - v.x * sin(angle));
}

vec2 rotate2(in vec2 v, in vec2 rotation) {
    vec2 rot = normalize(rotation);
    return vec2(v.x * rot.x - v.y * rot.y,
                v.y * rot.x - v.x * rot.y);
}

void main(void) {
    
    vec2 x = textureCoord;
    vec2 ce = end - center;
    vec2 cx = x - center;
    
    float cosF = dot(ce, cx) / (length(ce) * length(cx));
    float dX = sqrt(1.0 - cosF*cosF) * length(cx); // distance between x and main direction
    float dY = cosF * length(cx);// distance between x and Y axis
    
    float dYCoeff = gauss(dY, 0.2);
    
    vec2 shift = rotate(normalize(ce) * gauss(dX, 0.08) * dYCoeff * forceCoeff, atan(ce.x, ce.y));
    //vec2 shift = rotate(gauss2(dX, 0.08) * dYCoeff * forceCoeff, atan(ce.x, ce.y));
    //highp vec2 shift = gauss2(dX, 0.2) * dYCoeff * forceCoeff;
    

    gl_FragColor = texture2D(texture, x + shift);
}