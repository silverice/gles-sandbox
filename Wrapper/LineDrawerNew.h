//
//  LineDrawerNew.h
//  Wrapper
//
//  Created by Denis Halabuzar on 10/14/13.
//  Copyright (c) 2013 IDAP Group. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IDPDrawable.h"

@interface LineDrawerNew : NSObject <IDrawable>

@end
