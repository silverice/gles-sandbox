attribute vec4 Position; 
attribute vec4 SourceColor; 
 
varying lowp vec4 DestinationColor; 
 
uniform mat4 Projection;
 
//attribute vec2 TexCoordIn; // New
//varying vec2 TexCoordOut; // New
 
void main(void) { 
    DestinationColor = SourceColor; 
    gl_Position = Projection * Position;
}