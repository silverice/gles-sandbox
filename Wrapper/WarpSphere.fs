//
//  TextureDrawer.fs
//  Wrapper
//
//  Created by Denis Halabuzar on 2/12/13.
//  Copyright (c) 2013 IDAP Group. All rights reserved.
//

precision highp float;

uniform sampler2D texture;

varying highp vec2 textureCoord;

uniform highp vec2 center;
uniform highp vec2 end;

highp float forceCoeff = 1.0;

float radiusX = 0.15;
float radiusY = 0.3;

float sphere(in float x, in float radius) {
    if (abs(x) >= radius)
        return 0.0;
    else
        return sqrt(radius*radius - x*x);
}

vec2 sphere2(in float x, in float radius) {
    if (abs(x) >= radius)
        return vec2(0.0, 0.0);
    
    float h = sphere(x, radius);
    float tangent = 1.0 / (2.0 * h) * -2.0 * x;
    
    float angle = atan(tangent);
    return vec2((1.0 - cos(angle)) * h / tangent, h);
}

vec2 rotate(in vec2 v, in float angle) {
    // tan = sin / cos, dy is sin
    return vec2(v.x * cos(angle) - v.y * sin(angle),
                v.y * cos(angle) - v.x * sin(angle));
}

vec2 rotate2(in vec2 v, in vec2 rotation) {
    vec2 rot = normalize(rotation);
    return vec2(v.x * rot.x - v.y * rot.y,
                v.y * rot.x - v.x * rot.y);
}

void main(void) {
    
    vec2 x = textureCoord;
    vec2 ce = end - center;
    vec2 cx = x - center;
    
    float cosF = dot(ce, cx) / (length(ce) * length(cx));
    float dX = sqrt(1.0 - cosF*cosF) * length(cx); // distance coeff between x and main direction (x and X axis)
    float dY = cosF * length(cx); // distance between x and Y axis
    
    float dYCoeff = sphere(dY, 0.2);
    
    bool inRange = true;//distanceNorm < radius &&
                     //length(cx) < length(ce);

    
    vec2 shift = rotate(sphere2(dX, 0.2) * dYCoeff * forceCoeff, atan(ce.x, ce.y));
    //vec2 shift = gauss2(dX, 0.2) * dYCoeff * forceCoeff;
    
    if (inRange)
        gl_FragColor = texture2D(texture, x + shift);
    else
        gl_FragColor = texture2D(texture, x);
}