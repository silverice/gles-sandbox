//
//  IDPGLTexture.h
//  Wrapper
//
//  Created by Denis Halabuzar on 2/11/13.
//  Copyright (c) 2013 IDAP Group. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GLKit/GLKit.h>

// 2D texture
@interface IDPGLTexture : NSObject

- (GLuint)uId;
- (GLKVector2)size;

+ (void)bytes:(Byte *)bytes size:(CGSize)size toFile:(NSString *)path;


+ (id)objectWithImagePath:(NSString *)path;
+ (id)objectWithBytes:(Byte *)bytes size:(GLKVector2)size format:(GLenum)format;

- (void)updateData:(Byte *)bytes;
- (void)bind;
- (void)unbind;

@end
