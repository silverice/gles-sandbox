precision highp float;

const int kernelLength = 25;

uniform sampler2D texture;
//uniform sampler2D PSF;
uniform mediump float PSF[kernelLength];
uniform mediump vec2 textureSize;
uniform int kernelOffset;

//uniform int kernelLengthDyn;

varying highp vec2 textureCoord;

highp vec4 getPixel(in highp vec2 coords, in highp float dx, in highp float dy) {
	return texture2D(texture, coords + vec2(dx, dy));
}

highp vec4 getData() {
    int leftOffset = (-1)*kernelOffset;
    int rightOffset = kernelOffset + 1;
    int kernelItr = 0;
    
    highp vec4 summ = vec4(0.0, 0.0, 0.0, 0.0);
	for (int i = leftOffset; i < rightOffset; ++i) {
		for(int j = leftOffset; j < rightOffset; ++j) {
            summ += (PSF[kernelItr++] * getPixel(textureCoord, float(i)*textureSize.x, float(j)*textureSize.y));
      	}
   	}
    
    //return summ;
    
   	return vec4(clamp(summ.r, 0.0, 1.0),
                clamp(summ.g, 0.0, 1.0),
                clamp(summ.b, 0.0, 1.0),
                1.0);
}

void main() {
    gl_FragColor = getData();//mix( getData(), texture2D(texture, TexCoordOut), 0.5);
    //gl_FragColor = texture2D(texture, textureCoord);
}
