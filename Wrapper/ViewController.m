//
//  ViewController.m
//  Wrapper
//
//  Created by Denis Halabuzar on 2/8/13.
//  Copyright (c) 2013 IDAP Group. All rights reserved.
//

#import "ViewController.h"
#import "IDPShader.h"

#import "IDPParticleEffect.h"
#import "IDPWaves.h"

#import "MovingPyramide.h"
#import "Warp.h"
#import "TriangleGrad.h"
#import "LensEffect.h"
#import "BlurDrawer.h"
#import "LineDrawerNew.h"

#define BUFFER_OFFSET(i) ((char *)NULL + (i))



@interface ViewController () {   
    GLKMatrix4 _modelViewProjectionMatrix;
    GLKMatrix3 _normalMatrix;
    float _rotation;
    
    GLuint _vertexArray;
    GLuint _vertexBuffer;
}
@property (strong, nonatomic)   EAGLContext     *context;
@property (strong, nonatomic)   GLKBaseEffect   *effect;
@property (nonatomic, retain)   NSArray         *drawers;

- (void)setupGL;
- (void)tearDownGL;

@end

@implementation ViewController


@synthesize drawers     = _drawers;

- (void)dealloc
{
    [self tearDownGL];
    
    if ([EAGLContext currentContext] == self.context) {
        [EAGLContext setCurrentContext:nil];
    }
    
    self.context = nil;
    self.drawers = nil;
    [super dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.context = [[[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2] autorelease];

    if (!self.context) {
        NSLog(@"Failed to create ES context");
    }
    
    GLKView *view = (GLKView *)self.view;
    view.context = self.context;
    view.drawableDepthFormat = GLKViewDrawableDepthFormat24;
    
    [self setupGL];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

    if ([self isViewLoaded] && ([[self view] window] == nil)) {
        self.view = nil;
        
        [self tearDownGL];
        
        if ([EAGLContext currentContext] == self.context) {
            [EAGLContext setCurrentContext:nil];
        }
        self.context = nil;
    }

    // Dispose of any resources that can be recreated.
}

- (void)setupGL
{
    [EAGLContext setCurrentContext:self.context];
    
//    self.drawer = [[IDPLineDrawer new] autorelease];
//    [self.drawer drawDefault];
    
    self.drawers = @[
        //[IDPWaves object],
        [IDPParticleEffect object],
        //[BlurDrawer object],
        //[MovingPyramide object],
        //[LineDrawerNew object],
        //[IDPLineDrawer2 object],
        //[Warp object],
        //[LensEffect object],
        //[TriangleGrad object],
    ];
    
    //glEnable(GL_DEPTH_TEST);
}

- (void)tearDownGL {
    [EAGLContext setCurrentContext:self.context];
    
    for (id<IDrawable> drawable in self.drawers) {
        [drawable dispose];
    }
    
    glDeleteBuffers(1, &_vertexBuffer);
    glDeleteVertexArraysOES(1, &_vertexArray);
    
    self.effect = nil;
}

#pragma mark - GLKView and GLKViewController delegate methods

- (void)update
{
//    float aspect = fabsf(self.view.bounds.size.width / self.view.bounds.size.height);
//    GLKMatrix4 projectionMatrix = GLKMatrix4MakePerspective(GLKMathDegreesToRadians(65.0f), aspect, 0.1f, 100.0f);
//    
//    self.effect.transform.projectionMatrix = projectionMatrix;
//    
//    GLKMatrix4 baseModelViewMatrix = GLKMatrix4MakeTranslation(0.0f, 0.0f, -4.0f);
//    baseModelViewMatrix = GLKMatrix4Rotate(baseModelViewMatrix, _rotation, 0.0f, 1.0f, 0.0f);
//    
//    // Compute the model view matrix for the object rendered with GLKit
//    GLKMatrix4 modelViewMatrix = GLKMatrix4MakeTranslation(0.0f, 0.0f, -1.5f);
//    modelViewMatrix = GLKMatrix4Rotate(modelViewMatrix, _rotation, 1.0f, 1.0f, 1.0f);
//    modelViewMatrix = GLKMatrix4Multiply(baseModelViewMatrix, modelViewMatrix);
//    
//    self.effect.transform.modelviewMatrix = modelViewMatrix;
//    
//    // Compute the model view matrix for the object rendered with ES2
//    modelViewMatrix = GLKMatrix4MakeTranslation(0.0f, 0.0f, 1.5f);
//    modelViewMatrix = GLKMatrix4Rotate(modelViewMatrix, _rotation, 1.0f, 1.0f, 1.0f);
//    modelViewMatrix = GLKMatrix4Multiply(baseModelViewMatrix, modelViewMatrix);
//    
//    _normalMatrix = GLKMatrix3InvertAndTranspose(GLKMatrix4GetMatrix3(modelViewMatrix), NULL);
//    
//    _modelViewProjectionMatrix = GLKMatrix4Multiply(projectionMatrix, modelViewMatrix);
//    
//    _rotation += self.timeSinceLastUpdate * 0.5f;
    
//    GLKMatrix4 ortho = GLKMatrix4MakeOrtho(0, CGRectGetWidth(self.view.frame),
//                                           0, CGRectGetHeight(self.view.frame),
//                                           1, -1);
//    _modelViewProjectionMatrix = ortho;
}

- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect
{
    checkGL
    glClearColor(0, 0, 0, 1.0f);
    checkGL
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    checkGL
    
    
    //[self.drawer draw];
    
    for (id<IDrawable> drawable in self.drawers) {
        [drawable draw];
    }
}

#pragma mark - OpenGL ES 2 shader compilation

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    CGPoint point = [touches.anyObject locationInView:self.view];
    point.x = point.x / CGRectGetWidth(self.view.frame);
    point.y = (1 - point.y / CGRectGetHeight(self.view.frame));
    for (id<IDrawable> drawable in self.drawers) {
        if ([drawable respondsToSelector:@selector(onTouch:)]) {
            [drawable onTouch:point];
        }
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    CGPoint point = [touches.anyObject locationInView:self.view];
    point.x = point.x / CGRectGetWidth(self.view.frame);
    point.y = (1 - point.y / CGRectGetHeight(self.view.frame));
    for (id<IDrawable> drawable in self.drawers) {
        if ([drawable respondsToSelector:@selector(onTouchBegin:)]) {
            [drawable onTouchBegin:point];
        }
    }
}

@end
