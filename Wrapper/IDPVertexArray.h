//
//  IDPVertexArray.h
//  Wrapper
//
//  Created by Denis Halabuzar on 9/19/13.
//  Copyright (c) 2013 IDAP Group. All rights reserved.
//

#import <Foundation/Foundation.h>

// the data that contains vertices
@interface IDPBufferObject : NSObject

- (GLuint)uId;
- (void)bind;

- (void)modifyData:(const GLvoid *)data
          withSize:(GLsizei)size
          atOffset:(GLintptr)offset;

- (void)setData:(const GLvoid *)data
       withSize:(GLsizei)size
      withUsage:(GLenum)usage;

+ (id)objectWithUsage:(GLenum)usage
                 data:(const GLvoid *)data
                 size:(GLsizei)size;

+ (id)objectWithUsage:(GLenum)usage;

@end

//typedef struct {
//    
//} IDPVertexArrayIDPVertexArrayStructDecription;

// the data that gets passed into vertex shader
// vertex array with underlying Buffer object
@interface IDPVertexArray : NSObject

typedef struct {
    GLint           identifier;
    GLsizei         elementCount;
    GLenum          elementType;
    GLboolean       normalized;
    const GLvoid    *ptrOffset;
    
} IDPVertexArrayStructDecription;

// setup
// draw type (static, dynamic)
// put data of some kind

- (NSUInteger)vertexCount;
- (IDPBufferObject *)buffer;

+ (id)objectWithUsage:(GLenum)usage
                 data:(const GLvoid *)data
             dataSize:(GLsizei)dataSize
          elementSize:(GLsizei)elementSize;

- (void)describeStructWithIdentifier:(GLint)identifier
                        elementCount:(GLsizei)size
                         elementType:(GLenum)type
                          normalized:(GLboolean)normalized
                           ptrOffset:(const GLvoid *)ptrOffset;

- (void)describeStructures:(const IDPVertexArrayStructDecription *)descriptors
               structCount:(NSUInteger)count;



// usage
- (GLuint)uId;

- (void)bind;
- (void)bindAndDraw;
// GL_POINTS, GL_TRIENGLE_STRIP and etc
- (void)draw:(GLenum)mode;
- (void)draw:(GLenum)mode from:(NSUInteger)from count:(NSUInteger)count;

@end
