precision  highp float;

varying highp vec2 textureCoord;

highp vec2 center = vec2(0,0);

highp vec4 colorA = vec4(0.0, 0.0, 0.0, 1.0);
highp vec4 colorB = vec4(0.0,0.8, 0.4, 1.0);

float radius = 0.3;

highp vec4 gray(in vec4 col) {
    float val = dot(vec4(0.2126,0.7152,0.0722,1.0), col);
    return vec4(val,val,val,val);
}

void main(void) {

    bool inRange = true;//(distance(center, textureCoord) < radius);
    
    highp vec2 diff = textureCoord - center;
    const float PI = 3.14159265358979323846264;
    float coeff = (0.5 + atan(diff.y, diff.x) / PI);
        
    gl_FragColor = (colorA + (colorB - colorA) * coeff) * float(inRange) + vec4(0.,0.,0.,0.);
}