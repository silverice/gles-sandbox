//
//  IDPLineDrawer2.m
//  Wrapper
//
//  Created by Denis Halabuzar on 10/1/13.
//  Copyright (c) 2013 IDAP Group. All rights reserved.
//

#import "IDPParticleEffect.h"


@interface IDPParticleEffect ()
@property (retain)  GLProgram       *shader;
@property (retain)  GLVertexArray   *array;
@property (retain)  GLTexture       *texture;

@end

enum {
    Angle,
    Velocity,
    kSize,
};

typedef struct {
    float angle;
    float velocity;
    float size;
} Particle;

static const GLVertexArrayStructDescription ParticleDescr[] = {
    Velocity, 1, GLDataFloat, NO, offsetof(Particle, velocity),
    Angle, 1, GLDataFloat, NO, offsetof(Particle, angle),
    kSize, 1, GLDataFloat, NO, offsetof(Particle, size),
};

@implementation IDPParticleEffect {
    Particle _particles[7000];
    float _time;
}

- (void)dealloc {
    [self dispose];
    [super dealloc];
}

- (void)dispose {
    self.shader = nil;
    self.array = nil;
    self.texture = nil;
}

static float uniform() {
    return (rand() % 10000 / 10000.f);
}

static float randUniform(float min, float max) {
    return min + (max - min) * uniform();
}

static float randUniformSq(float min, float max) {
    float uni = uniform();
    return min + (max - min) * uni * uni;
}

- (id)init {
    self = [super init];
    if (self) {
        self.shader = [GLProgram objectWithVertShaderName:@"LineDrawer2.vs"
                                               fragShader:@"LineDrawer2.fs"
                                     linkedWithAttributes:@[@"angle", @(Angle),
                                                            @"velocity", @(Velocity),
                                                            @"size", @(kSize)]];

        glEnable(GL_BLEND);
        glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
        
        CGSize rect = [[UIScreen mainScreen] bounds].size;
        rect.width  *= 1;
        rect.height *= 1;
        checkGL
        GLKMatrix4 proj = GLKMatrix4MakeOrtho(-rect.width/2, rect.width/2, -rect.height/2, rect.height/2, 1, -1);
        [self.shader setUniform:@"Projection" toMatrix4fv:1 transpose:NO value:proj.m];
        
        self.texture = [GLTexture objectWithImageAtPath:@"Particle.png"];

        checkGL
        for (int i = 0; i < countOf(_particles); ++i) {
            float angleRand = (rand() % 10000 / 10000.f);
            double velocityRand = (rand() % 10000 / 10000.f);

            _particles[i].angle = 2 * M_PI * angleRand;
            _particles[i].velocity = velocityRand * velocityRand * 70;
            _particles[i].size = randUniformSq(2, 5);
        }
        _time = 0;
        
        self.array = [GLVertexArray objectWithUsage:GLBufferUsageStaticDraw
                                                data:_particles
                                            dataSize:sizeof(_particles)
                                         elementSize:elementSize(_particles)];
        checkGL
        [self.array describeStructures:ParticleDescr
                           structCount:countOf(ParticleDescr)];
        
 
        checkGL
    }
    return self;
}

- (void)draw {
    
    _time += 1 / 30.f;
    
//    if (_time > 10) {
//        _time = 0;
//    }
    
    [self.texture ensureActive];
    
    [self.shader bind];
    
    [self.shader setUniform:@"texture" to1i:self.texture.slotIndex];
    [self.shader setUniform:@"time" to1f:_time];
    
    [self.array draw:GLPrimitivePoints from:0 count:self.array.vertexCount];
    checkGL

    [self.shader unbind];
}

@end
