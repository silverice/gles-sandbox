precision highp float;

varying mediump vec4 colorV;
varying mediump vec2 vertCoordV;

uniform sampler2D texture;
uniform vec2 bounds;

void main(void) {
    //gl_FragColor = colorV;
    gl_FragColor = texture2D(texture, vertCoordV / bounds) * vec4(1.,1.,1., colorV.a);
    //gl_FragColor = colorV;
    //gl_FragColor = vec4(0.,1.,1.,colorV.a);
}
