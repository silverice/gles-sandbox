//
//  IDPGLTexture.m
//  Wrapper
//
//  Created by Denis Halabuzar on 2/11/13.
//  Copyright (c) 2013 IDAP Group. All rights reserved.
//

#import "IDPGLTexture.h"

@interface IDPGLTexture ()
@property (nonatomic, assign)   GLuint      uId;
@property (nonatomic, assign)   GLKVector2  size;

@end

@implementation IDPGLTexture

@synthesize uId     = _uId;
@synthesize size    = _size;

+ (void)bytes:(Byte *)bytes size:(CGSize)size toFile:(NSString *)path {
}

- (id)init {
    self = [super init];
    if (self) {
        glGenTextures(1, &_uId);
    }
    return self;
}

- (void)dealloc {
    glDeleteTextures(1, &_uId);
    [super dealloc];
}

+ (id)objectWithImagePath:(NSString *)path {
    IDPGLTexture *texture = [self object];
    if (texture) {
        NSString *res = [[NSBundle mainBundle] pathForResource:path
                                                        ofType:nil];
        
        
        //res = @"/Users/vilya/Documents/Developer/Tests/Wrapper/Wrapper/imgres.png";
        
        UIImage *img = [UIImage imageWithContentsOfFile:res];
        assert(img);
        CGImageRef image = img.CGImage;
        assert(image);
        
        size_t width = CGImageGetWidth(image);
        size_t height = CGImageGetHeight(image);
        
        GLubyte * spriteData = (GLubyte *) calloc(width*height*4, sizeof(GLubyte));
        
        CGContextRef spriteContext = CGBitmapContextCreate(spriteData, width, height, 8, width*4,
                                                           CGImageGetColorSpace(image), kCGImageAlphaPremultipliedLast);
        
        // 3
        CGContextDrawImage(spriteContext, CGRectMake(0, 0, width, height), image);
        
        CGContextRelease(spriteContext);
        
        // 4
        glBindTexture(GL_TEXTURE_2D, texture.uId);
        
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, spriteData);
        
        free(spriteData);
        
        texture.size = GLKVector2Make(width, height);
    }
    
    return texture;
}

+ (id)objectWithBytes:(Byte *)bytes size:(GLKVector2)size format:(GLenum)format {
    IDPGLTexture *texture = [self object];
    if (texture) {
        glBindTexture(GL_TEXTURE_2D, texture.uId);
        
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, size.x, size.y, 0, format, GL_UNSIGNED_BYTE, bytes);
    }
    return texture;
}

- (void)bind {
    glBindTexture(GL_TEXTURE_2D, self.uId);
}

- (void)unbind {
    glBindTexture(GL_TEXTURE_2D, 0);
}

- (void)updateData:(Byte *)bytes {
    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, self.size.x, self.size.y, GL_RGBA, GL_UNSIGNED_BYTE, bytes);
}

@end
