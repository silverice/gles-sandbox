//
//  NSObject+Ext.m
//  Wrapper
//
//  Created by Denis Halabuzar on 9/19/13.
//  Copyright (c) 2013 IDAP Group. All rights reserved.
//

#import "NSObject+Ext.h"

@implementation NSObject (Ext)

+ (id)object {
    return [[[self alloc] init] autorelease];
}

- (id)autocopy {
    return [[self copy] autorelease];
}

- (id)autocopymutable {
    return [[self mutableCopy] autorelease];
}

@end
