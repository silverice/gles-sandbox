//
//  IDPSymbolVertexGen.h
//  Wrapper
//
//  Created by Denis Halabuzar on 10/14/13.
//  Copyright (c) 2013 IDAP Group. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef struct {
    GLfloat r, g, b, a;
} STColor4;

typedef struct {
    GLfloat x, y;
    STColor4 color;
} STSymbolVertice;

// Generates point & color vertices
@interface STSymbolVertexGen : NSObject

// byte array with STSymbolVertice structures
+ (NSMutableData *)createTriangleMeshFromPoints:(const CGPoint *)points
                                     pointCount:(GLuint)pointCount
                                      lineWidth:(CGFloat)lineWidth
                               transparentWidth:(CGFloat)transparentWidth;

@end
