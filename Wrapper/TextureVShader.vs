attribute vec4 Position; 
attribute vec2 TexCoord;
//attribute vec4 SourceColor;

varying highp vec2 textureCoord;
 
uniform mat4 Projection;
 
void main(void) {
    textureCoord = TexCoord;
    gl_Position = Projection * Position;
}