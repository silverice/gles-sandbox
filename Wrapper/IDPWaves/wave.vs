attribute vec2 point;

precision highp float;

uniform mat4 proj;
uniform float time;

void main(void) {
    gl_Position = proj * vec4(point.x, point.y, 0, 1.);
}
