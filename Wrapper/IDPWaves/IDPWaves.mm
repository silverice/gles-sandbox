//
//  IDPWaves.m
//  Wrapper
//
//  Created by Denis Halabuzar on 12/6/13.
//  Copyright (c) 2013 IDAP Group. All rights reserved.
//

#import "IDPWaves.h"
#import "vec2.h"

typedef struct {
    vec2 f, m, l;
    
    vec2& vertice (int i) {
        assert(i < 3);
        return *(&f + i);
    }
    
} Triangle;

typedef struct {
    Triangle a, b;
} Quad;

static inline Triangle STTriangleStrip(const Triangle& tri, vec2 l) {
    return (Triangle){tri.m, tri.l, l};
}

static inline Triangle STTriangleMake(vec2 f, vec2 m, vec2 l) {
    return (Triangle){f, m, l};
}

static NSMutableData *makeSurface(int w, int h, float size) {
    NSMutableData *data = [NSMutableData data];
    
    for (int iW = 0; iW < w; ++iW) {
        for (int iH = 0; iH < h; ++iH) {
            
            vec2 first = vec2(iW, iH) * size;
            
            Triangle tri1 = STTriangleMake(first, first + vec2(size, 0), first + vec2(0, size));
            Triangle tri2 = STTriangleStrip(tri1, first + vec2(size, size));
            
            [data appendBytes:&tri1 length:sizeof(Triangle)];
            [data appendBytes:&tri2 length:sizeof(Triangle)];
        }
    }
    
    return data;
}

@interface IDPWaves ()
@property (retain)  GLVertexArray   *wavesData;
@property (retain)  GLProgram       *program;

@end

@implementation IDPWaves

- (id)init
{
    self = [super init];
    if (self) {
        self.program = [GLProgram objectWithVertShaderName:@"wave.vs"
                                                fragShader:@"wave.fs"];
        
        GLProgramAttrib2Loc assoc[] = {"point", 0};
        [self.program associateAttributes:assoc
                                    count:1];
        [self.program link];
        
        
        NSMutableData *verts = makeSurface(10, 10, 0.5);
        
        self.wavesData = [GLVertexArray objectWithUsage:GLBufferUsageStaticDraw
                                                   data:verts.bytes
                                               dataSize:verts.length
                                            elementSize:sizeof(vec2)];
        
        GLVertexArrayStructDescription desc[] = {
            0, 2, GLDataFloat, NO, 0,
        };
        
        [self.wavesData describeStructures:desc structCount:1];
        
        GLKMatrix4 proj = GLKMatrix4MakePerspective(60 / 180.f * M_PI , 2/5.f, 0.01, 100);
        
        GLKMatrix4 modelViewMatrix = GLKMatrix4MakeScale(1/6., 1/6., 1/6.);
        modelViewMatrix = GLKMatrix4Multiply(modelViewMatrix, GLKMatrix4MakeRotation(80 / 180.f * M_PI, 1, 0, 0));
        
        GLKMatrix4 res = GLKMatrix4Multiply(modelViewMatrix, GLKMatrix4Identity);
        
        //GLKMatrix4 proj = GLKMatrix4MakeOrtho(0, 10, 0, 10, -1, 1);
        
        [self.program setUniform:@"proj"
                     toMatrix4fv:1
                       transpose:NO
                           value:res.m];
    }
    return self;
}

- (void)draw {
    [self.program bind];
    [self.wavesData draw:GLPrimitiveTriangles];
    [self.program unbind];
}

@end
