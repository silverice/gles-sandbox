//
//  IDPWaves.h
//  Wrapper
//
//  Created by Denis Halabuzar on 12/6/13.
//  Copyright (c) 2013 IDAP Group. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IDPDrawable.h"

@interface IDPWaves : NSObject <IDrawable>

@end
