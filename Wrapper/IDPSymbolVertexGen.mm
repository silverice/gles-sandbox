//
//  IDPSymbolVertexGen.m
//  Wrapper
//
//  Created by Denis Halabuzar on 10/14/13.
//  Copyright (c) 2013 IDAP Group. All rights reserved.
//

#import "IDPSymbolVertexGen.h"


#define static_assert(expr) typedef char MERGE_MACRO(static_assert_failed_at_, __LINE__) [(expr) ? 1 : -1];
#define MERGE_MACRO(x, y) x##y

typedef struct {
    GLfloat x, y;
} STPoint2d;

typedef struct {
    vec2 point;
    STColor4 color;
} STVertice;

struct STLineSegment {
    STVertice a, b, c, d, e, f, g;
    
    void setFirstMesh(const STColor4 &transparentColor) {
        a.color = transparentColor;
        c.color = transparentColor;
        e.color = transparentColor;
        g.color = transparentColor;
    }
    
    void setLastMesh(const STColor4 &transparentColor) {
        b.color = transparentColor;
        d.color = transparentColor;
        f.color = transparentColor;
    }
    
    void connectToPrev(STLineSegment& prev) {
        c.point = prev.f.point = (c.point + prev.f.point) * 0.5f;
        e.point = prev.d.point = (e.point + prev.d.point) * 0.5f;
        g.point = prev.b.point = (g.point + prev.b.point) * 0.5f;
    }
};

vec2 STRotateVec(vec2 vec, float angle) {
    return vec2(vec.x * cosf(angle) - vec.y * sinf(angle),
                vec.x * sinf(angle) + vec.y * cosf(angle));
}

struct STEndPoint {
    STVertice
    a, b, c, d, e, f, g, h,
    dupF,
    center,
    dupD,
    dupB;
    
    void setupFor(float lineWidht, float transpWidth, CGPoint prevP, CGPoint lastP) {
        vec2 prev = vec2(prevP.x, prevP.y);
        vec2 last = vec2(lastP.x, lastP.y);
        
        vec2 dir = normalize(last - prev);
        vec2 ab = STRotateVec(dir, M_PI / 2);
        vec2 cd = STRotateVec(ab, M_PI / 3);
        vec2 ef = STRotateVec(cd, M_PI / 3);
        vec2 gh = STRotateVec(ef, M_PI / 3);
        
        float radius = lineWidht * 0.5  + transpWidth;
        
        vec2 cursor = a.point = last - ab * radius;
        b.point = (cursor += ab * transpWidth);
        c.point = (cursor = last - cd * radius);
        d.point = (cursor = last - cd * lineWidht * 0.5);
        f.point = (cursor = last - ef * lineWidht * 0.5);
        e.point = (cursor = last - ef * radius);
        g.point = (cursor = last - gh * radius);
        h.point = (cursor = last - gh * lineWidht * 0.5);
        
        center.point = last;
        
        STColor4 transparentColor{0,0,0,0};
        a.color = transparentColor;
        c.color = transparentColor;
        e.color = transparentColor;
        g.color = transparentColor;
        
        STColor4 color{0,0,0,1};
        b.color = color;
        d.color = color;
        f.color = color;
        h.color = color;
        center.color = color;
        
        // copy into duplicates
        dupF = f;
        dupD = d;
        dupB = b;
    }
};

enum {
    STSymbolVerticesPerSegment = sizeof(STLineSegment) / sizeof(STVertice),
};

static_assert(sizeof(STPoint2d) == 4*2);
static_assert(sizeof(STColor4) == 4*4);
static_assert(sizeof(vec2) == 8);
static_assert(sizeof(STVertice) == (4*2 + 4*4));
static_assert(sizeof(STLineSegment) == (7 * (4*2 + 4*4)));

@implementation STSymbolVertexGen

static inline void STPushSegment(NSMutableData *data, STLineSegment line) {
    [data appendBytes:&line length:sizeof(line)];
}

static inline void STPushPoint(NSMutableData *data, STSymbolVertice vertice) {
    [data appendBytes:&vertice length:sizeof(vertice)];
}

+ (NSMutableData *)createTriangleMeshFromPoints:(const CGPoint *)points
                                     pointCount:(GLuint)pointCount
                                      lineWidth:(CGFloat)lineWidth
                               transparentWidth:(CGFloat)transparentWidth
{
    assert(points && pointCount >= 1);
    
    GLuint meshCount = (pointCount - 1);
    
    //STLineSegment *meshs = (STLineSegment *)malloc(meshCount * sizeof(STLineSegment));
    
    NSMutableData *data = [NSMutableData dataWithCapacity:meshCount * sizeof(STLineSegment)];
    
    GLfloat aliasedWidth = transparentWidth;
    
    STColor4 lineColor{0,1,1,1};
    STColor4 transparentColor{0,0,0,0};
    
    GLuint meshItr = 0;
    
    for (GLuint i = 0; i < (pointCount - 1); ++i) {
        vec2 prev = vec2(points[i].x, points[i].y);
        vec2 next = vec2(points[i + 1].x, points[i + 1].y);
        
        bool down = ((i % 2) == 0);
        vec2 ab = next - prev;
        vec2 downDir = normalize(vec2(-ab.y, ab.x)) * (down ? 1 : -1);
        vec2 cBig = downDir * lineWidth - ab;
        vec2 cSmall = downDir * aliasedWidth - ab;
        vec2 first = prev - downDir * (lineWidth * 0.5 + aliasedWidth);
        
        STLineSegment mesh = {
            .a.point = first,
            .b.point = (first += ab),
            .c.point = (first += cSmall),
            .d.point = (first += ab),
            .e.point = (first += cBig),
            .f.point = (first += ab),
            .g.point = (first += cSmall),
            
            .a.color = transparentColor,
            .b.color = transparentColor,
            .c.color = lineColor,
            .d.color = lineColor,
            .e.color = lineColor,
            .f.color = lineColor,
            .g.color = transparentColor,
        };
        
        if (meshItr > 0) {
            mesh.connectToPrev(*(STLineSegment *)((char *)data.bytes + data.length - sizeof(STLineSegment)));
        }
        
        STPushSegment(data, mesh);
        
        ++meshItr;
    }

    
    ((STLineSegment *)data.bytes)->setFirstMesh(transparentColor);
    ((STLineSegment *)((char *)data.bytes + data.length - sizeof(STLineSegment)))->setLastMesh(transparentColor);
    
    //meshs[meshCount - 1].setLastMesh(transparentColor);
    
    //*verticesOut = (STSymbolVertice *)meshs;
    //*verticeCount = meshItr * STSymbolVerticesPerSegment;
    
    
//    STEndPoint end;
//    end.setupFor(lineWidth, transparentWidth, points[pointCount - 2], points[pointCount - 1]);
//    [data appendBytes:&end length:sizeof(STEndPoint)];
    
    
    return data;
}

@end
