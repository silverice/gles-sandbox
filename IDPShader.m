//
//  IDPShaderProgram.m
//  iOSPaint
//
//  Created by Denis Halabuzar on 2/8/13.
//  Copyright (c) 2013 IDAP Group. All rights reserved.
//

#import "IDPShader.h"

@interface IDPShader ()
@property (nonatomic, assign)   GLuint      uId;
@property (nonatomic, assign)   GLuint      vertexSh;
@property (nonatomic, assign)   GLuint      fragmentSh;

- (GLuint)compileShaderOfType:(GLenum)type file:(NSString *)file;

@end

@implementation IDPShader

@synthesize uId       = _programId;
@synthesize vertexSh        = _vertexSh;
@synthesize fragmentSh      = _fragmentSh;

- (id)initWithVertexShader:(NSString *)vertexShader
            fragmentShader:(NSString *)fragmentShader
{
    self = [super init];
    if (self) {
        self.vertexSh = [self compileShaderOfType:GL_VERTEX_SHADER
                                             file:vertexShader];
        self.fragmentSh = [self compileShaderOfType:GL_FRAGMENT_SHADER
                                               file:fragmentShader];
        
        self.uId = glCreateProgram();
        
        glAttachShader(self.uId, self.vertexSh);
        glAttachShader(self.uId, self.fragmentSh);
        
        // mark shader for deletion
        glDeleteShader(self.vertexSh);
        glDeleteShader(self.fragmentSh);
    }
    
    return self;
}

- (void)detachShaders {
    glDetachShader(self.uId, self.vertexSh);
    self.vertexSh = 0;
    glDetachShader(self.uId, self.fragmentSh);
    self.fragmentSh = 0;
}

- (void)dealloc {
    [self detachShaders];
    glDeleteProgram(self.uId);
    [super dealloc];
}

#pragma mark - Public methods

- (void)use {
    glUseProgram(self.uId);
}

- (void)unuse {
    glUseProgram(0);
}

- (void)bindAttribute:(const GLchar *)name toLocation:(GLuint)location {
    assert(location < GL_MAX_VERTEX_ATTRIBS);
    glBindAttribLocation(self.uId, location, name);
}

- (GLuint)attribLocation:(const GLchar *)name {
    GLuint location = glGetAttribLocation(self.uId, name);
    assert(location != -1);
    return location;
}

- (void)link {
    glLinkProgram(self.uId);
    
    GLint logLength;
    glGetProgramiv(self.uId, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetProgramInfoLog(self.uId, logLength, &logLength, log);
        NSLog(@"Program link log:\n%s", log);
        free(log);
    }
    
    GLint status;
    glGetProgramiv(self.uId, GL_LINK_STATUS, &status);
    assert(status == GL_TRUE);
    
    [self detachShaders];
}

- (GLuint)uniformLocation:(const GLchar *)uniform {
    GLuint location = glGetUniformLocation(self.uId, uniform);
    assert(location != -1);
    return location;
}

- (GLuint)compileShaderOfType:(GLenum)type file:(NSString *)file {
    GLuint shader = 0;
    GLint status;
    const GLchar *source;
    
    NSString *vertPath = [[NSBundle mainBundle] pathForResource:file
                                                         ofType:(type == GL_VERTEX_SHADER ? @"vs" : @"fs")];
    
    source = (GLchar *)[[NSString stringWithContentsOfFile:vertPath
                                                  encoding:NSUTF8StringEncoding
                                                     error:nil] UTF8String];
    if (!source) {
        NSLog(@"Failed to load shader %@", file);
        assert(false);
    }
    
    shader = glCreateShader(type);
    glShaderSource(shader, 1, &source, NULL);
    glCompileShader(shader);
    
    GLint logLength;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetShaderInfoLog(shader, logLength, &logLength, log);
        NSLog(@"Shader '%@' compile log:\n%s", file, log);
        free(log);
    }
    
    glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
    if (status == 0) {
        glDeleteShader(shader);
        shader = 0;
        assert(false);
    }
    
    return shader;
}

@end
