//
//  IDPShaderProgram.h
//  iOSPaint
//
//  Created by Denis Halabuzar on 2/8/13.
//  Copyright (c) 2013 IDAP Group. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <OpenGLES/EAGL.h>

@class IDPVertexShader;
@class IDPFrameShader;

@interface IDPShader : NSObject

@property (nonatomic, assign, readonly)   GLuint  uId;

- (id)initWithVertexShader:(NSString *)vertexShader
            fragmentShader:(NSString *)fragmentShader;

// setup:
- (void)bindAttribute:(const GLchar *)name toLocation:(GLuint)location;
- (void)link;
- (GLuint)uniformLocation:(const GLchar *)uniform;

//
- (GLuint)attribLocation:(const GLchar *)name;
- (void)use;
- (void)unuse;

@end
